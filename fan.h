#ifndef FAN_H
#define FAN_H
#include <stdio.h>
#include <stdbool.h>

#define FAN_PWM_CONTROL "/sys/class/hwmon/hwmon3/pwm%d"
#define FAN_RPM "/sys/class/hwmon/hwmon3/fan%d_input"
#define FAN_PWM_CONTROL_ENABLED "/sys/class/hwmon/hwmon3/pwm%d_enable"

enum FAN_NUMBER
{
    FIRST_FAN = 1,
    SECOND_FAN = 2
};

enum FAN_MODES
{
    MAX_SPEED,
    MANUAL,
    AUTO
};

bool    fan_set_level                   (int             pwm_val, enum FAN_NUMBER fan_number);
bool    fan_is_manual_control_enabled   (enum FAN_NUMBER fan_number);
bool    fan_set_control_mode            (enum FAN_NUMBER fan_number, enum FAN_MODES mode);
int     fan_get_rpm                     (enum FAN_NUMBER fan_number);



#endif
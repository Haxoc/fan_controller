#ifndef TEMPERATURE_H
#define TEMPERATURE_H

#define CPU_SENSOR_TEMP_INPUT "/sys/class/hwmon/hwmon4/temp%d_input"
#define CPU_SENSOR_TEMP_LABEL "/sys/class/hwmon/hwmon4/temp%d_label"

extern int temp_table[17];

typedef enum CPU_TEMP
{
	PACKAGE = 1,
	CORE_0 = 2,
	CORE_1 = 3
} CPU_TEMP;




double get_cpu_temp(CPU_TEMP temp_type);

#endif
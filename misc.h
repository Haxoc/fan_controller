#ifndef MISC_H
#define MISC_H

#define FAN_LEVEL_SCALE 7


char *get_formatted_filename(char *str_to_format, int number_to_format);
int levels_to_pwm(int level, int scale);
int get_fan_pwm_from_temp(double temp_c, int curr_fan_level, int *temp_table, int temp_table_len);

#endif
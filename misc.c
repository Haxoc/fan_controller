#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "misc.h"

char *
get_formatted_filename(char *str_to_format, 
                       int   number_to_format)
{
    size_t chars_to_add = 0;

    if (number_to_format > 9 && number_to_format < 100)
    {
        chars_to_add = 1;
    }

    char *filename = (char *)malloc(sizeof(char) * (strlen(str_to_format) + chars_to_add));
    sprintf(filename, str_to_format, number_to_format);
    return filename;
}

int 
levels_to_pwm(int level, 
                  int scale)
{
	return (int)round(level * 255 / scale);
}

// returns pwm for what the fan level needs to be for a given temp
int 
get_fan_pwm_from_temp(double  temp_c, 
                      int     curr_fan_level, 
                      int    *temp_table, 
                      int     temp_table_len)
{
	// default fan level of 2
	static int fan_level = 2;
	// start at index 1 to start at a temprature not fan level
	int i = 1;
	while (i < temp_table_len - 1 && temp_c > temp_table[i])
	{
		i += 2;
	}
	fan_level = temp_table[i + 1];

    if(fan_level != curr_fan_level)
    {

    }

	return levels_to_pwm(fan_level, FAN_LEVEL_SCALE);
}
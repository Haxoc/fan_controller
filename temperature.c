#include <stdlib.h>
#include <stdio.h>

#include "temperature.h"
#include "misc.h"

// in every double block
// first  will be the fan speed in levels (0,7)
// second will be the temp in degrees C
int temp_table[17] = {
	0,
	50, 1,
	55, 2,
	60, 3,
	65, 4,
	75, 5,
	80, 6,
	85, 7};

double 
get_cpu_temp(enum CPU_TEMP temp_type)
{
	char *filename = get_formatted_filename(CPU_SENSOR_TEMP_INPUT, temp_type);
	FILE *temp_file = fopen(filename, "r");
	int return_temp = 55000;

	if (temp_file != NULL)
	{
		fscanf(temp_file, "%d", &return_temp);
		fclose(temp_file);
	}

	free(filename);
	return return_temp / 1000.0;
}


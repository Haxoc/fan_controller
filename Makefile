CFLAGS = -Wall -Wextra -Wshadow -Werror -pedantic-errors -Wchar-subscripts -Winit-self -Wmissing-include-dirs -Wunused -Wconversion

fan_control: fancontrol.o fan.o misc.o temperature.o
	cc $^ -o $@  

fancontrol.o: fancontrol.c 

fan.o: fan.c

misc.o: misc.c

temperature.o: temperature.c

clean:
	rm -f fancontrol *.o



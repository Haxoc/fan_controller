#include <stdlib.h>
#include "fan.h"
#include "misc.h"
#include <stdbool.h>

bool fan_is_manual_control_enabled(enum FAN_NUMBER fan_number)
{
    char *filename = get_formatted_filename(FAN_PWM_CONTROL_ENABLED, fan_number);
    FILE *pwm_enable_file = fopen(filename, "r");
    int fanmode = 2;

    if (pwm_enable_file != NULL)
    {
        fscanf(pwm_enable_file, "%d", &fanmode);
        fclose(pwm_enable_file);
    }
    else
    {
        printf("The address %s is wrong\n", filename);
        printf("Couldn't check for manual control!\n");
    }

    free(filename);
    return fanmode == MANUAL;
}

bool fan_set_level(int pwm_val,
                   enum FAN_NUMBER fan_number)
{
    // If there are 10 fans or more to control with pwm, we would need 1 extra character in the file address
    if (pwm_val > 255 || pwm_val < 0)
    {
        return false;
    }

    char *filename = get_formatted_filename(FAN_PWM_CONTROL, fan_number);
    FILE *pwm_write_file = fopen(filename, "w");
    bool success = true;

    if (pwm_write_file != NULL)
    {
        fprintf(pwm_write_file, "%d", pwm_val);
        fclose(pwm_write_file);
        printf("Set fan %d level to %d\n", fan_number, pwm_val);
    }
    else
    {
        printf("The address %s is wrong OR not elevated privilages\n", filename);
        printf("Couldn't set fan level\n");
        success = false;
    }
    free(filename);
    return success;
}

bool fan_set_control_mode(enum FAN_NUMBER fan_number, enum FAN_MODES mode)
{
    char *filename = get_formatted_filename(FAN_PWM_CONTROL_ENABLED, fan_number);
    FILE *pwm_write_file = fopen(filename, "w");
    bool success = false;

    if (pwm_write_file != NULL)
    {
        fprintf(pwm_write_file, "%d", mode);
        fclose(pwm_write_file);
        success = true;
        printf("Fan %d: Changed control mode to %d\n", fan_number, mode);
    }
    else
    {
        printf("The address %s is wrong OR not elevated privilages\n", filename);
        printf("Couldn't set fan %d to mode %d\n", fan_number, mode);
    }
    free(filename);

    return success;
}

int fan_get_rpm(enum FAN_NUMBER fan_number)
{
    char *filename = get_formatted_filename(FAN_RPM, fan_number);
    FILE *pwm_rpm_file = fopen(filename, "r");
    int rpm = -1;

    if (pwm_rpm_file != NULL)
    {
        fscanf(pwm_rpm_file, "%d", &rpm);
        fclose(pwm_rpm_file);
    }
    else
    {
        printf("The address %s of the rpm file is wrong\n", filename);
        printf("Couldn't get fan rpm!\n");
    }

    free(filename);

    return rpm;
}
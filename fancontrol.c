#include <stdlib.h>
#include <unistd.h>

#include "temperature.h"
#include "misc.h"
#include "fan.h"

int main(int argc, char **argv)
{
	if(argc == 1)
	{
		printf("disabling manual fan control!\n");
		if (!fan_set_control_mode(FIRST_FAN, AUTO))
		{
			printf("FAILED!\n");
			return -1;
		}
		printf("SUCCESS!\n");
		return 0;
	}

	int fan_level_arg = atoi(argv[1]);

	if(fan_level_arg > FAN_LEVEL_SCALE || fan_level_arg < 0)
	{
		printf("Fan level is [0,7] only. Not %d\n", fan_level_arg);
		printf("FAILED!\n");
		return -1;
	}
	
	printf("Setting fan to manual control\n");
	if (!fan_set_control_mode(FIRST_FAN, MANUAL))
	{
		printf("FAILED!\n");
		return -1;
	}

	printf("Setting fan level\n");
	if(!fan_set_level(levels_to_pwm(fan_level_arg, FAN_LEVEL_SCALE), FIRST_FAN))
	{
		printf("FAILED!\n");
		printf("Disabling manual fan control\n");
		fan_set_control_mode(FIRST_FAN, AUTO);
		return -1;
	}

	printf("SUCCESS!\n");

	return 0;
}
## Linux Kernel and Sensors
The linux kernel has a standard to control and monitor sensors, it is called sysfs and it replaces procfs. I think that Thinkfan uses the old procfs to control the fan but sysfs to monitor temperatures. My program will strictly use sysfs as procfs is deprecated. In order to find the sensors and try and control the fans I looked in the Linux kernel documentation for [sysfs naming standards](https://www.kernel.org/doc/html/latest/hwmon/sysfs-interface.html) as well as the specific [thinkpad-acpi](https://www.kernel.org/doc/html/latest/admin-guide/laptops/thinkpad-acpi.html) driver. The temperature is given in milidegree celsius because the system does not want to deal with floating point numbers and the fan is controlled with pwm integers [0, 255] although I know, from previous experience that the fan only has 8 levels including stopped [0, 7] which means I am going to convert from pwm to levels at some point in the program. All the sensors are at “/sys/class/hwmon/hwmon#” where # is an integer [0, max]. On my system the fan control is at hwmon3/, the core temps are at hwmon4/ ac adapter is at hwmon0/, the chipset temperature is at hwmon1/ and the battery is hwmon2/. You can tell which is which based on the file “name” that is inside in each of these directories.

## Enabling Fan Control
In order to enable fan control you need to put "options thinkpad_acpi fan_control=1" in the file /usr/lib/modprobe.d/thinkpad_acpi.conf if it does not exist will need to create it, as root obviously. 

After this, reboot and change the number in pwm1_enable from 2 to 1 [0-max, 1-manual, 2-bios]. This will now read the integer from the file pwm1 as the fan level, it is default to 255, but you can change it.  
*Note:* The program changes pwm1_enable on its own, so it is not necessary to do this step if you will be using the program.

Keep in mind that the fan is only set to 8 levels so the exact number does not make that much of a difference.

## How the program works:
- change pwm1_enable to 1 [0-max, 1-manual, 2-bios]
- Now we can change the pwm value in pwm1 [0, 255] -> [stopped, max]
- in hwmon/hwmon3 temp1\_input is the cpu, temp2\_input is nvidia gpu units is milicelsius
- hwmon/hwmon4 is for cpu core temps temp#\_label is the name package or core 0-7 (7 would be an 8 core cpu)
- we could compare the temp#_input to its "crit" for automatic fan control
- chipset temp label is "acpitz"
